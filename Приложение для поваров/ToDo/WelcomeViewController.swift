//
//  WelcomeViewController.swift
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import UIKit
import RealmSwift

class WelcomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        title = "Добро пожаловать"
        
        if let _ = SyncUser.current {
            self.navigationController?.viewControllers = [ItemsViewController()]
            //self.navigationController?.pushViewController(ItemsViewController(), animated: true)
        } else {
            let alertController = UIAlertController(title: "Войти в систему", message: "Введите данные", preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: "Войти", style: .default, handler: { [unowned self]
                alert -> Void in
                let textField = alertController.textFields![0] as UITextField
                let textField1 = alertController.textFields![1] as UITextField
                let textField2 = alertController.textFields![2] as UITextField
                let creds = SyncCredentials.usernamePassword(username: textField.text!, password: textField1.text!, register: false)
                UserDefaults.standard.set(textField.text, forKey: "nickname")
                UserDefaults.standard.set(textField1.text, forKey: "password")
                UserDefaults.standard.set(textField2.text, forKey: "phoneNumber")
                
                SyncUser.logIn(with: creds, server: Constants.AUTH_URL, onCompletion: { [weak self](user, err) in
                    if let _ = user {
                        self?.navigationController?.viewControllers = [ItemsViewController()]
                        //self?.navigationController?.pushViewController(ProjectsViewController(), animated: true)
                    } else if let error = err {
                        self?.makeAlert()
                    }
                })
            }))
            alertController.addAction(UIAlertAction(title: "Регистрация", style: .default, handler: { [unowned self]
                alert -> Void in
                let textField = alertController.textFields![0] as UITextField
                let textField1 = alertController.textFields![1] as UITextField
                let textField2 = alertController.textFields![2] as UITextField
                let creds = SyncCredentials.usernamePassword(username: textField.text!, password: textField1.text!, register: true)
                UserDefaults.standard.set(textField.text, forKey: "nickname")
                UserDefaults.standard.set(textField1.text, forKey: "password")
                UserDefaults.standard.set(textField2.text, forKey: "phoneNumber")
                
                SyncUser.logIn(with: creds, server: Constants.AUTH_URL, onCompletion: { [weak self](user, err) in
                    if let _ = user {
                        self?.navigationController?.pushViewController(ItemsViewController(), animated: true)
                    } else if let error = err {
                        self?.makeAlert()
                    }
                })
            }))
            alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
                textField.placeholder = "Имя вашего пользователя"
            })
            alertController.addTextField { (textField1) in
                textField1.isSecureTextEntry = true
                textField1.placeholder = "Пароль"
            }
            alertController.addTextField { (textField2) in
                textField2.placeholder = "Номер телефона"
            }
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func makeAlert() {
        let alertController = UIAlertController(title: "Ошибка!", message: "Попробуйте еще раз", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { (action) in
            self.viewDidAppear(true)
        }))
        self.present(alertController, animated: true)
    }
    
}

