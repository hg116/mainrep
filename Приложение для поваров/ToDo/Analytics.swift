//
//  Analytics.swift
//  Приложение для водителей
//
//  Created by Евгений Рубанов on 10/12/18.
//

//
//  Analytics.swift
//  ToDo
//
//  Created by Евгений Рубанов on 10/11/18.
//

import RealmSwift

class Analytics: Object {
    
    @objc dynamic var projectId: String = UUID().uuidString
    @objc dynamic var adress: String = ""
    @objc dynamic var cooker: String = "Ожидание..."
    @objc dynamic var rider: String = "Ожидание..."
    @objc dynamic var name: String = ""
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "projectId"
    }
    
}

