//
//  Item.swift
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import RealmSwift

class Item: Object {
    
    @objc dynamic var itemId: String = UUID().uuidString
    @objc dynamic var body: String = ""
    @objc dynamic var isDone: Bool = false
    @objc dynamic var coordinatesFrom: String = ""
    @objc dynamic var coordinatesWhere: String = ""
    @objc dynamic var cookerReady: Bool = false
    @objc dynamic var weigth: Int = 0
    @objc dynamic var status: String = "В ожидании..."
    @objc dynamic var isDoneByCooker: Bool = false
    @objc dynamic var reciepe: String = ""
    @objc dynamic var nickname: String = ""
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
    
}

