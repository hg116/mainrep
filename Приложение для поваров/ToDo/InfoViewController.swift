//
//  InfoViewController.swift
//  Приложение для водителей
//
//  Created by Евгений Рубанов on 14.09.2018.
//

import UIKit
import RealmSwift
import MapKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    let realm: Realm
    let items: Results<Item>
    
    @IBAction func accept(_ sender: Any) {
        try! self.realm.write {
            let item = items[indexPath]
            item.isDone = !item.isDone
            item.nickname = UserDefaults.standard.string(forKey: "nickname")!
        }
    }
    
    var indexPath = 0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(Item.self).sorted(byKeyPath: "timestamp", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeViewController(nameStr: String, fromStr: String, whereStr: String, index: Int) {
        nameLabel.text = nameStr
        indexPath = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
}
