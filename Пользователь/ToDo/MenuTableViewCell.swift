//
//  MenuTableViewCell.swift
//  Администрационная панель
//
//  Created by Евгений Рубанов on 20.09.2018.
//

import UIKit
import RealmSwift

class MenuTableViewCell: UITableViewCell {
    
    let realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)))
    let items = try! Realm(configuration: Realm.Configuration(syncConfiguration: SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL))).objects(Item.self).sorted(byKeyPath: "timestamp", ascending: false)
    
    var selectedIndex = 0

//    @IBOutlet weak var makeOrderButton: UIButton!
    
    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var weigthLabel: UILabel!
    
    @IBOutlet weak var infoLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    //    @IBAction func buttonTapped(_ sender: Any) {
//        makeOrderButton.titleLabel?.text = "Заказано"
//        makeOrderButton.titleLabel?.backgroundColor = .gray
//        makeOrderButton.isUserInteractionEnabled = false
//        try! realm.write {
//            items[selectedIndex].isDone = true
//        }
//    }
    
    func makeCell(imageMain: UIImage, strName: String, strWeigth: String, strInfo: String, status: String, selIn: Int) {
        self.menuImage.image = imageMain
        self.nameLabel.text = strName
        self.weigthLabel.text = strWeigth
        self.infoLabel.text = strInfo
        self.statusLabel.text = status
        self.selectedIndex = selIn
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
