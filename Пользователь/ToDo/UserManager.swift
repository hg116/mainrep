//
//  UserManager.swift
//  ToDo
//
//  Created by Евгений Рубанов on 10/12/18.
//

import RealmSwift

class UserManager: Object {
    
    @objc dynamic var itemId: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var password: String = ""
    @objc dynamic var phoneNumber: String = ""
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
    
}

