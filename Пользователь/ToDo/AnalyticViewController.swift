//
//  AnalyticViewController.swift
//  ToDo
//
//  Created by Евгений Рубанов on 10/12/18.
//

import UIKit

class AnalyticViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var cookerLabel: UILabel!
    
    @IBOutlet weak var riderLabel: UILabel!
    
    @IBOutlet weak var adressLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    func makeVC(name: String, date: Date, cooker: String, rider: String, adress: String) {
        nameLabel.text = name
        dateLabel.text = "\(date)"
        cookerLabel.text = cooker
        riderLabel.text = rider
        adressLabel.text = adress
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
