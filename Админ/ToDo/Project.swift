//
//  Project.swift
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import RealmSwift

class Project: Object {
    
    @objc dynamic var projectId: String = UUID().uuidString
    @objc dynamic var owner: String = ""
    @objc dynamic var name: String = ""
    @objc dynamic var timestamp: Date = Date()

    let items = List<Item>()
    
    override static func primaryKey() -> String? {
        return "projectId"
    }
    
}
