//
//  ProjectsViewController.swift
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import UIKit
import RealmSwift

class ProjectsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let realm: Realm
    let projects: Results<Project>
    var notificationToken: NotificationToken?
    var subscriptionToken: NotificationToken?
    var subscription: SyncSubscription<Project>!
    
    var tableView = UITableView()
    let activityIndicator = UIActivityIndicatorView()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        realm = try! Realm(configuration: SyncConfiguration.automatic())
        
        projects = realm.objects(Project.self).filter("owner = %@", SyncUser.current!.identity!).sorted(byKeyPath: "timestamp", ascending: false)
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Подразделения"
        view.addSubview(tableView)
        view.addSubview(activityIndicator)
        activityIndicator.center = self.view.center
        activityIndicator.color = .darkGray
        activityIndicator.isHidden = false
        activityIndicator.hidesWhenStopped = true
        
        tableView.frame = self.view.frame
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItemButtonDidClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Выйти", style: .plain, target: self, action: #selector(logoutButtonDidClick))
        
        subscription = projects.subscribe(named: "my-projects")
        
        activityIndicator.startAnimating()
        subscriptionToken = subscription.observe(\.state, options: .initial) { state in
            if state == .complete {
                self.activityIndicator.stopAnimating()
            } else {
                print("Subscription State: \(state)")
            }
        }
        
        
        notificationToken = projects.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
    }
    
    deinit {
        notificationToken?.invalidate()
        subscriptionToken?.invalidate()
        activityIndicator.stopAnimating()
    }
    
    @objc func addItemButtonDidClick() {
        let alertController = UIAlertController(title: "Добавить новое направление", message: "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            let project = Project()
            project.name = textField.text ?? ""
            project.owner = SyncUser.current!.identity!
            try! self.realm.write {
                self.realm.add(project)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Название нового объекта"
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func logoutButtonDidClick() {
        let alertController = UIAlertController(title: "Выйти", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Да, выйти", style: .destructive, handler: {
            alert -> Void in
            SyncUser.current?.logOut()
            self.navigationController?.setViewControllers([WelcomeViewController()], animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.selectionStyle = .none
        let project = projects[indexPath.row]
        cell.textLabel?.text = project.name
        cell.detailTextLabel?.text = project.items.count > 0 ? "\(project.items.count) задач" : "Нет задач"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let project = projects[indexPath.row]
        let itemsVC = ItemsViewController()
        itemsVC.project = project
        self.navigationController?.pushViewController(itemsVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let project = projects[indexPath.row]
        if project.items.count > 0 {
            confirmDeleteProjectAndTasks(project: project)
        } else {
            deleteProject(project)
        }
    }
    
    @objc func confirmDeleteProjectAndTasks(project: Project) {
        let alertController = UIAlertController(title: "Удалить \(project.name)?", message: "Удалятся \(project.items.count) задания", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Да, удалить \(project.name)", style: .destructive, handler: {
            alert -> Void in
            self.deleteProject(project)
        }))
        alertController.addAction(UIAlertAction(title: "отменить", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }

    func deleteProject(_ project:Project) {
        try! realm.write {
            realm.delete(project.items)
            realm.delete(project)
        }
    }
    
}

