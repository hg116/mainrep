//
//  Products.swift
//  Администрационная панель
//
//  Created by Евгений Рубанов on 14.09.2018.
//

import RealmSwift

class Products: Object {
    
    @objc dynamic var itemId: String = UUID().uuidString
    @objc dynamic var body: Bool = true
    @objc dynamic var name: String = ""
    @objc dynamic var count: Int = 0
    @objc dynamic var timestamp: Date = Date()
    @objc dynamic var timeDelete: Int = 0
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
    
}
