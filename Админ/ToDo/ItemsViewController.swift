//
//  ItemsViewController
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import UIKit
import RealmSwift

class ItemsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    
    let realm: Realm
    let items: Results<Item>
    var pickerArray: [String] = []
    let products: Results<Products>
//    let users: Results<UserManager>
    let analytic: Results<Analytics>
    
    var project: Project?
    
    var notificationToken: NotificationToken?
    var tableView = UITableView()
    
    let pickerView = UIView(frame: UIScreen.main.bounds)
    var viewMy: UIView!
    var picker: UIPickerView!
//    var textFieldFrom: UITextField!
    var textFieldWhere: UITextField!
    var buttonReady: UIButton!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(Item.self).sorted(byKeyPath: "timestamp", ascending: false)
        self.products = realm.objects(Products.self).sorted(byKeyPath: "timestamp", ascending: false)
        self.analytic = realm.objects(Analytics.self).sorted(byKeyPath: "timestamp", ascending: false)
//        self.users = realm.objects(UserManager.self).sorted(byKeyPath: "timestamp", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerArray[row]
    }
    
    var rowMy = 0
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.rowMy = row
    }
    
    func makePickerArray() {
        var pr1 = "Помидор"
        var pr2 = "Мука"
        var pr3 = "Масло"
        var countPr = 0
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Пицца")
        }
        countPr = 0
        pr1 = "Рыба"
        pr2 = "Рис"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Суши")
        }
        countPr = 0
        pr1 = "Рыба"
        pr2 = "Рис"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            }
        }
        if countPr == 2 {
            pickerArray.append("Чизкейк")
        }
        countPr = 0
        pr1 = "Креветки"
        pr2 = "Рис"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Цезарь с креветками")
        }
        countPr = 0
        pr1 = "Апельсин"
        pr2 = "Сахар"
        pr3 = "Вода"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Апельсиновый фрэш")
        }
        countPr = 0
        pr1 = "Вода"
        pr2 = "Мука"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Мивина")
        }
        countPr = 0
        pr1 = "Вода"
        pr2 = "Мука"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Одноразовые приборы")
        }
        countPr = 0
        pr1 = "Рис"
        pr2 = "Мука"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Плов")
        }
        countPr = 0
        pr1 = "Мясо"
        pr2 = "Мука"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Стейк")
        }
        countPr = 0
        pr1 = "Мясо"
        pr2 = "Мука"
        pr3 = "Масло"
        for i in 0..<products.count {
            let obj = products[i]
            if obj.name == pr1 {
                countPr += 1
            } else if obj.name == pr2 {
                countPr += 1
            } else if obj.name == pr3 {
                countPr += 1
            }
        }
        if countPr == 3 {
            pickerArray.append("Бургер")
        }
        countPr = 0
    }
    
    @objc func openPicker() {
        makePickerArray()
        picker.reloadAllComponents()
        UIView.animate(withDuration: 0.5, animations: {
            self.viewMy.alpha = 1
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Заказы"
        view.addSubview(tableView)
        tableView.frame = self.view.frame
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
//        try! realm.write {
//            let obj = UserManager()
//            obj.name = UserDefaults.standard.value(forKey: "nickname") as! String
//            obj.password = UserDefaults.standard.value(forKey: "password") as! String
//            obj.phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as! String
//            realm.add(obj)
//        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(openPicker))
        
        notificationToken = items.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
        
        var rectPicker = CGRect(x: 0.0, y: pickerView.bounds.height - 300.0, width: pickerView.bounds.width, height: 300.0)
        let rectButton = CGRect(x: 10.0, y: 5, width: 100, height: 100)
        buttonReady = UIButton(frame: rectButton)
        viewMy = UIView(frame: CGRect(x: 0.0, y: pickerView.bounds.height - 600.0, width: pickerView.bounds.width, height: 600.0))
        //        textFieldFrom = UITextField(frame: CGRect(x: 0.0, y: pickerView.bounds.height - 500.0, width: pickerView.bounds.width, height: 50.0))
        //        textFieldFrom.isUserInteractionEnabled = true
        //        textFieldFrom.backgroundColor = .white
        //        textFieldFrom.placeholder = "Откуда"
        //        textFieldFrom.borderStyle = .roundedRect
        textFieldWhere = UITextField(frame: CGRect(x: 0.0, y: pickerView.bounds.height - 500.0, width: pickerView.bounds.width, height: 50.0))
        textFieldWhere.isUserInteractionEnabled = true
        textFieldWhere.backgroundColor = .white
        textFieldWhere.placeholder = "Куда"
        textFieldWhere.borderStyle = .roundedRect
        
        rectPicker.origin.y = 40
        rectPicker.size.height = rectPicker.size.height - rectPicker.origin.y
        picker = UIPickerView(frame: rectPicker)
        
        self.viewMy.backgroundColor = UIColor.lightGray
        pickerView.backgroundColor = .red
        buttonReady.tintColor = .blue
        buttonReady.setTitle("Готово", for: .normal)
        self.viewMy.alpha = 0
        
        self.tabBarController?.view.addSubview(self.viewMy)
        self.viewMy.addSubview(self.picker)
        self.viewMy.addSubview(self.buttonReady)
        //        self.viewMy.addSubview(self.textFieldFrom)
        self.viewMy.addSubview(self.textFieldWhere)
        
        picker.delegate = self
        picker.dataSource = self
        
        MenuTableViewCell.register(for: tableView)
        
        buttonReady.addTarget(self, action: #selector(closePicker), for: .touchUpInside)
    }
    
    @objc func closePicker() {
        if rowMy >= pickerArray.count {
            if pickerArray != [] {
                if pickerArray[rowMy] == "Пицца" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Помидор" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.body = pickerArray[rowMy] ?? ""
                        item.weigth = 250
                        item.reciepe = "Мука, помидор, масло"
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Суши" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рыба" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Рис, рыба, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Чизкейк" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 450
                        item.reciepe = "Мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Цезарь с креветками" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Креветки" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 150
                        item.reciepe = "Рис, креветки, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Апельсиновый фрэш" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Апельсин" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Сахар" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Вода, апельсин, сахар"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Мивина" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Вода"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Одноразовые приборы" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Пластик"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Плов" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Рис, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Стейк" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мясо" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Мясо, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                } else if pickerArray[rowMy] == "Бургер" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мясо" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = Item()
                        item.weigth = 200
                        item.reciepe = "Мясо, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        realm.add(item)
                    }
                }
            }
        } else {
            try! realm.write {
                if pickerArray[rowMy] == "Пицца" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Помидор" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.body = pickerArray[rowMy] ?? ""
                        item.weigth = 250
                        item.reciepe = "Мука, помидор, масло"
                    }
                } else if pickerArray[rowMy] == "Суши" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рыба" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Рис, рыба, масло"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Чизкейк" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 450
                        item.reciepe = "Мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Цезарь с креветками" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Креветки" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 150
                        item.reciepe = "Рис, креветки, масло"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Апельсиновый фрэш" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Апельсин" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Сахар" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Вода, апельсин, сахар"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Мивина" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Вода"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Одноразовые приборы" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Вода" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Пластик"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Плов" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Рис" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Рис, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Стейк" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мясо" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Мясо, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""
                    }
                } else if pickerArray[rowMy] == "Бургер" {
                    try! realm.write {
                        for obj in products {
                            if obj.name == "Мясо" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Мука" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            } else if obj.name == "Масло" {
                                obj.count -= 1
                                if obj.count < 1 {
                                    realm.delete(obj)
                                }
                            }
                        }
                        var item = items[rowMy]
                        item.coordinatesFrom = "Харьков"
                        item.coordinatesWhere = textFieldWhere.text!
                        item.weigth = 200
                        item.reciepe = "Мясо, мука, масло"
                        item.body = pickerArray[rowMy] ?? ""                    }
                }
            }
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewMy.alpha = 0
        })
        pickerArray = []
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
//    @objc func rightBarButtonDidClick() {
//        let alertController = UIAlertController(title: "Добавить", message: "", preferredStyle: .alert)
//
//        alertController.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
//            alert -> Void in
//            let textField = alertController.textFields![0] as UITextField
//            let textField1 = alertController.textFields![1] as UITextField
//            let textField2 = alertController.textFields![2] as UITextField
//            let item = Item()
//            item.body = textField.text ?? ""
//            item.coordinatesFrom = textField1.text ?? ""
//            item.coordinatesWhere = textField2.text ?? ""
//            try! self.realm.write {
//                self.realm.add(item)
//            }
//        }))
//        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
//        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
//            textField.placeholder = "Название новой задачи"
//        })
//        alertController.addTextField { (textField1 : UITextField!) in
//            textField1.placeholder = "Откуда"
//        }
//        alertController.addTextField { (textField2 : UITextField!) in
//            textField2.placeholder = "Куда"
//        }
//        self.present(alertController, animated: true, completion: nil)
//    }
    
//    @objc func rightBarButtonDidClick() {
//        let alertController = UIAlertController(title: "Выйти", message: "", preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: "Да, выйти", style: .destructive, handler: {
//            alert -> Void in
//            SyncUser.current?.logOut()
//            self.navigationController?.setViewControllers([WelcomeViewController()], animated: true)
//        }))
//        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle , reuseIdentifier: "Cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        let item = items[indexPath.row]
        cell.makeCell(imageMain: UIImage(named: "\(item.body)")!, strName: item.body, strWeigth: "\(item.weigth) г", strInfo: item.reciepe)
//        cell.textLabel?.text = item.body
//        cell.accessoryType = item.isDone ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
//        cell.detailTextLabel?.text = item.isDone ? "Принял: \(item.nickname)" : ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        openPicker()
//        textFieldWhere.text = items[indexPath.row].coordinatesWhere
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let item = items[indexPath.row]
        try! realm.write {
            realm.delete(item)
        }
    }
    
    
}

extension UITableViewCell {
    
    class func register(for tableView:UITableView) {
        let reuseId =  String(describing:self)
        let cellNib = UINib(nibName: reuseId, bundle: Bundle(for: self))
        tableView.register(cellNib, forCellReuseIdentifier: reuseId)
    }
    
    class func deque<T: UITableViewCell>(from tableView:UITableView) -> T {
        let reuseId =  String(describing:self)
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseId),
            type(of:cell) == self
            else {
                fatalError()
        }
        return cell as! T
    }
    
}

