//
//  ProductsTableViewController.swift
//  Администрационная панель
//
//  Created by Евгений Рубанов on 14.09.2018.
//

import UIKit
import RealmSwift

class ProductsTableViewController: UITableViewController {
    
    let realm: Realm
    let products: Results<Products>
    
    var notificationToken: NotificationToken?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.products = realm.objects(Products.self).sorted(byKeyPath: "timestamp", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
        
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Склад"
        
        try! realm.write {
            let pr = Products()
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(rightBarButtonDidClick))
        
        notificationToken = products.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
    }
    
    @objc func rightBarButtonDidClick() {
        let alertController = UIAlertController(title: "Добавить", message: "", preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: {
            alert -> Void in
            let textField = alertController.textFields![0] as UITextField
            let textField1 = alertController.textFields![1] as UITextField
            let textField2 = alertController.textFields![2] as UITextField
            let product = Products()
            product.name = textField.text ?? ""
            product.count = Int(textField1.text!) ?? 0
            product.timeDelete = Int(textField2.text!) ?? 7
            try! self.realm.write {
                self.realm.add(product)
            }
        }))
        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "Название продукта"
        })
        alertController.addTextField { (textField1 : UITextField!) in
            textField1.placeholder = "Количество"
        }
        alertController.addTextField { (textField2: UITextField!) in
            textField2.placeholder = "Срок годности"
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func timeRemainingString(finishDate date:NSDate) -> String {
        let secondsFromNowToFinish = date.timeIntervalSinceNow
        let hours = Int(secondsFromNowToFinish / 3600)
        let minutes = Int((secondsFromNowToFinish - Double(hours) * 3600) / 60)
        let seconds = Int(secondsFromNowToFinish - Double(hours) * 3600 - Double(minutes) * 60 + 0.5)
        
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = products[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle , reuseIdentifier: "Cell")
        cell.accessoryType = product.body ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
        cell.textLabel?.text = product.name
        cell.detailTextLabel?.text = "Количество: \(product.count). Привезен: \(product.timestamp)"
        if Date().timeIntervalSince(product.timestamp) / 60 / 60 / 24 > Double(product.timeDelete - 1) {
            cell.backgroundColor = .yellow
            print(Date().timeIntervalSince(product.timestamp) / 60 / 60 / 24)
        } else if Date().timeIntervalSince(product.timestamp) / 60 / 60 / 24 > Double(product.timeDelete) {
            cell.backgroundColor = .red
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        let product = products[indexPath.row]
        try! realm.write {
            realm.delete(product)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return products.count
    }
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    
}
