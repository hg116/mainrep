//
//  Users.swift
//  Администрационная панель
//
//  Created by Евгений Рубанов on 18.09.2018.
//

import Foundation
import RealmSwift

class Users: Object {
    
    @objc dynamic var userId: String = UUID().uuidString
    @objc dynamic var isOnline: Bool = true
    @objc dynamic var imageUser = UIImage()
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
}
