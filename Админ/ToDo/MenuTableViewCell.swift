//
//  MenuTableViewCell.swift
//  Администрационная панель
//
//  Created by Евгений Рубанов on 20.09.2018.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var weigthLabel: UILabel!
    
    @IBOutlet weak var infoLabel: UILabel!
    
    func makeCell(imageMain: UIImage, strName: String, strWeigth: String, strInfo: String) {
        self.menuImage.image = imageMain
        self.nameLabel.text = strName
        self.weigthLabel.text = strWeigth
        self.infoLabel.text = strInfo
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
