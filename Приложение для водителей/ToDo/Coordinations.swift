//
//  CoordManager.swift
//  Приложение для водителей
//
//  Created by Евгений Рубанов on 10/11/18.
//

import RealmSwift

class Coordinations: Object {
    
    @objc dynamic var itemId: String = UUID().uuidString
    @objc dynamic var coordX: Double = 0
    @objc dynamic var coordY: Double = 0
    @objc dynamic var timestamp: Date = Date()
    
    override static func primaryKey() -> String? {
        return "itemId"
    }
    
}

