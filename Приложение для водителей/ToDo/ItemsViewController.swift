//
//  ItemsViewController.swift
//  Todo
//
//  Created by Евгений Рубанов on 05.09.2018.
//  Copyright © 2018 Евгений Рубанов. All rights reserved.
//

import UIKit
import RealmSwift
import CoreLocation
import MapKit

class ItemsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate {
    
    let realm: Realm
    let items: Results<Item>
    let coord: Results<Coordinations>
//    let users: Results<UserManager>
    let analytic: Results<Analytics>
    let locationManager = CLLocationManager()
    var yourLocation = CLLocation()
    
    var notificationToken: NotificationToken?
    var tableView = UITableView(frame: CGRect(), style: .grouped)
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(Item.self).sorted(byKeyPath: "timestamp", ascending: false)
        self.coord = realm.objects(Coordinations.self).sorted(byKeyPath: "timestamp", ascending: false)
        self.analytic = realm.objects(Analytics.self).sorted(byKeyPath: "timestamp", ascending: false)
//        self.users = realm.objects(UserManager.self).sorted(byKeyPath: "timestamp", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeGradient() {
        let color1 = UIColor(red: 153 / 255, green: 26 / 255, blue: 61 / 255, alpha: 1).cgColor
        let color2 = UIColor(red: 243 / 255, green: 155 / 255, blue: 51 / 255, alpha: 1).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [color1, color2]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        tableView.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.yourLocation = location
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Увози меня скорей"
        self.navigationController?.navigationBar.barTintColor = .orange
        tableView.backgroundColor = .white
        view.addSubview(tableView)
        tableView.frame = self.view.frame
        self.tableView.delegate = self
        self.tableView.dataSource = self
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Выйти", style: .plain, target: self, action: #selector(leftBarButtonDidClick))
        
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
//        try! realm.write {
//            let obj = Coordinations()
//            obj.coordX = yourLocation.coordinate.latitude
//            obj.coordY = yourLocation.coordinate.longitude
//        }
        
//        try! realm.write {
//            let obj = UserManager()
//            obj.name = UserDefaults.standard.value(forKey: "nickname") as! String
//            obj.password = UserDefaults.standard.value(forKey: "password") as! String
//            obj.phoneNumber = UserDefaults.standard.value(forKey: "phoneNumber") as! String
//            realm.add(obj)
//        }
        
        notificationToken = items.observe { [weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                fatalError("\(error)")
            }
        }
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    @objc func leftBarButtonDidClick() {
        let alertController = UIAlertController(title: "Выйти", message: "", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Да, выйти", style: .destructive, handler: {
            alert -> Void in
            SyncUser.current?.logOut()
//            try! self.realm.write {
//                for obj in self.users {
//                    if obj.name == UserDefaults.standard.value(forKey: "nickname") as! String, obj.password == UserDefaults.standard.value(forKey: "password") as! String , obj.phoneNumber == UserDefaults.standard.value(forKey: "nickname") as! String{
//                        self.realm.delete(obj)
//                    }
//                }
//            }
            self.navigationController?.setViewControllers([WelcomeViewController()], animated: true)
        }))
        alertController.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .subtitle, reuseIdentifier: "Cell")
        cell.backgroundColor = items[indexPath.row].isDoneByCooker ? UIColor.white : UIColor.red
        cell.textLabel?.textColor = .black
        cell.detailTextLabel?.textColor = .black
        cell.selectionStyle = .none
        let item = items[indexPath.row]
        cell.textLabel?.text = item.body
        //        cell.accessoryType = item.isDone ? UITableViewCellAccessoryType.checkmark : UITableViewCellAccessoryType.none
        cell.detailTextLabel?.text = item.isDone ? "Принял: \(item.nickname)" : ""
        if cell.detailTextLabel?.text != "" {
            if cell.detailTextLabel?.text?.replacingOccurrences(of: "Принял: ", with: "") != UserDefaults.standard.string(forKey: "nickname")! {
                cell.isUserInteractionEnabled = false
            }
        }
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        //        Timer.scheduledTimer(withTimeInterval: 180, repeats: false) { (timer) in
        //            if self.coord != nil {
        //                var s = 0
        //                for obj in self.items {
        //                    if obj.nickname == UserDefaults.standard.string(forKey: "nickname")! {
        //                        s = 1
        //                    }
        //                }
        //                self.geocode(item)
        //                var arr: [CLLocationDistance] = []
        //                let location1 = CLLocation(latitude: self.whereLatitude, longitude: self.whereLongitude)
        //                for obj in self.coord {
        //                    let location2 = CLLocation(latitude: obj.coordX, longitude: obj.coordY)
        //                    arr.append(location1.distance(from: location2))
        //                }
        //                arr = arr.sorted(by: { (s1, s2) -> Bool in
        //                    s1 < s2
        //                })
        //                if arr[0] == location1.distance(from: self.yourLocation) {
        //                    if s == 0 {
        //                        if item.isDone == false {
        //                            try! self.realm.write {
        //                                let item1 = self.items[indexPath.row]
        //                                item1.isDone = !item1.isDone
        //                                item1.nickname = UserDefaults.standard.string(forKey: "nickname")!
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 5
//    }
//
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView()
//        headerView.backgroundColor = UIColor.clear
//        return headerView
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        let vc = InfoViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        Timer.scheduledTimer(withTimeInterval: 0.01, repeats: false) { (timer) in
            vc.makeViewController(nameStr: item.body, fromStr: item.coordinatesFrom, whereStr: item.coordinatesWhere, index: indexPath.row)
        }
//        vc.makeViewController(nameStr: item.body, fromStr: item.coordinatesFrom, whereStr: item.coordinatesWhere)
    }
    
    let geocoderFrom = CLGeocoder()
    let geocoderWhere = CLGeocoder()
    
    var placeCount = 0
    
    var fromLatitude: Double = 0
    
    var fromLongitude: Double = 0
    
    var whereLatitude: Double = 0
    
    var whereLongitude: Double = 0
    
    func makeCoordinates() {
        placeCount += 1
        if placeCount == 2 {
            placeCount = 0
            let pathURL = "http://maps.apple.com/maps?saddr=\(fromLatitude),\(fromLongitude)&daddr=\(whereLatitude),\(whereLongitude)"
            if let url = NSURL(string: pathURL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func geocode(_ item: Item) {
        geocoderWhere.geocodeAddressString(item.coordinatesWhere) { (placemarksOptional, error) -> Void in
            
            if let placemarks = placemarksOptional {
                print("placemark| \(placemarks.first)")
                if let location = placemarks.first?.location {
                    self.whereLatitude = location.coordinate.latitude
                    self.whereLongitude = location.coordinate.longitude
                    self.makeCoordinates()
                } else {
                    // Could not get a location from the geocode request. Handle error.
                    self.makeAlert()
                }
            } else {
                // Didn't get any placemarks. Handle error.
                self.makeAlert()
            }
        }
    }
    
    func openMaps(_ indexPath: Int) {
        
        let item = items[indexPath]
        
        geocoderFrom.geocodeAddressString(item.coordinatesFrom) { (placemarksOptional, error) -> Void in
            if let placemarks = placemarksOptional {
                print("placemark| \(placemarks.first)")
                if let location = placemarks.first?.location {
                    self.fromLatitude = location.coordinate.latitude
                    self.fromLongitude = location.coordinate.longitude
                    self.makeCoordinates()
                } else {
                    // Could not get a location from the geocode request. Handle error.
                    self.makeAlert()
                }
            } else {
                // Didn't get any placemarks. Handle error.
                self.makeAlert()
            }
        }
        
        geocoderWhere.geocodeAddressString(item.coordinatesWhere) { (placemarksOptional, error) -> Void in
            
            if let placemarks = placemarksOptional {
                print("placemark| \(placemarks.first)")
                if let location = placemarks.first?.location {
                    self.whereLatitude = location.coordinate.latitude
                    self.whereLongitude = location.coordinate.longitude
                    self.makeCoordinates()
                } else {
                    // Could not get a location from the geocode request. Handle error.
                    self.makeAlert()
                }
            } else {
                // Didn't get any placemarks. Handle error.
                self.makeAlert()
            }
        }
    }
    
    func makeAlert() {
        let alertController = UIAlertController(title: "Ошибка!", message: "Невозможно проложить маршрут!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { (action) in
            
        }))
        self.present(alertController, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
//        
//        let makeMap = UITableViewRowAction(style: .normal, title: "Проложить маршрут") { (action, indexPath) in
//            try! self.realm.write {
//                let item = self.items[indexPath.row]
//                item.isDone = item.isDone ? true : true
//                item.nickname = UserDefaults.standard.string(forKey: "nickname")!
//            }
////            let vc = MapViewController()
////            vc.makeIndexPath(indexPath.row)
////            self.navigationController?.pushViewController(vc, animated: true)
//            
//            self.openMaps(indexPath.row)
//            
//        }
//        
//        return [makeMap]
//        
//    }
    


}

