//
//  InfoViewController.swift
//  Приложение для водителей
//
//  Created by Евгений Рубанов on 14.09.2018.
//

import UIKit
import RealmSwift
import MapKit

class InfoViewController: UIViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var fromLabel: UILabel!
    
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet weak var endButton: UIButton!
    
    @IBOutlet weak var cookerReady: UILabel!
    
    @IBOutlet weak var whereLabel: UILabel!
    
    @IBAction func makeRequest(_ sender: Any) {
        self.openMaps(indexPath)
    }
    
    let geocoderFrom = CLGeocoder()
    let geocoderWhere = CLGeocoder()
    
    var placeCount = 0
    
    var fromLatitude: Double = 0
    
    var fromLongitude: Double = 0
    
    var whereLatitude: Double = 0
    
    var whereLongitude: Double = 0
    
    func makeCoordinates() {
        placeCount += 1
        if placeCount == 2 {
            placeCount = 0
            let pathURL = "http://maps.apple.com/maps?saddr=\(fromLatitude),\(fromLongitude)&daddr=\(whereLatitude),\(whereLongitude)"
            if let url = NSURL(string: pathURL) {
                UIApplication.shared.openURL(url as URL)
            }
        }
    }
    
    func openMaps(_ indexPath: Int) {
        
        let item = items[indexPath]
        
        geocoderFrom.geocodeAddressString(item.coordinatesFrom) { (placemarksOptional, error) -> Void in
            if let placemarks = placemarksOptional {
                print("placemark| \(placemarks.first)")
                if let location = placemarks.first?.location {
                    self.fromLatitude = location.coordinate.latitude
                    self.fromLongitude = location.coordinate.longitude
                    self.makeCoordinates()
                } else {
                    // Could not get a location from the geocode request. Handle error.
                    self.makeAlert()
                }
            } else {
                // Didn't get any placemarks. Handle error.
                self.makeAlert()
            }
        }
        
        geocoderWhere.geocodeAddressString(item.coordinatesWhere) { (placemarksOptional, error) -> Void in
            
            if let placemarks = placemarksOptional {
                print("placemark| \(placemarks.first)")
                if let location = placemarks.first?.location {
                    self.whereLatitude = location.coordinate.latitude
                    self.whereLongitude = location.coordinate.longitude
                    self.makeCoordinates()
                } else {
                    self.makeAlert()
                }
            } else {
                self.makeAlert()
            }
        }
    }
    
    func makeAlert() {
        let alertController = UIAlertController(title: "Ошибка!", message: "Невозможно проложить маршрут!", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: { (action) in
            
        }))
        self.present(alertController, animated: true)
    }
    
    let realm: Realm
    let items: Results<Item>
    let analytic: Results<Analytics>
    
    @IBAction func accept(_ sender: Any) {
        try! self.realm.write {
            let item = items[indexPath]
            item.isDone = !item.isDone
            item.nickname = UserDefaults.standard.string(forKey: "nickname")!
            item.status = "Принял водитель \(UserDefaults.standard.value(forKey: "nickname") as! String)"
            let obj = analytic[indexPath]
            obj.rider = UserDefaults.standard.value(forKey: "nickname") as! String
        }
        acceptButton.isUserInteractionEnabled = false
        acceptButton.alpha = 0
        endButton.alpha = 1
    }
    @IBAction func endTapped(_ sender: Any) {
        try! realm.write {
            let obj = items[indexPath]
            realm.delete(obj)
        }
        navigationController?.popViewController(animated: true)
    }
    
    var indexPath = 0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: Constants.REALM_URL)
        self.realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig))
        self.items = realm.objects(Item.self).sorted(byKeyPath: "timestamp", ascending: false)
        self.analytic = realm.objects(Analytics.self).sorted(byKeyPath: "timestamp", ascending: false)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeViewController(nameStr: String, fromStr: String, whereStr: String, index: Int) {
        nameLabel.text = nameStr
        fromLabel.text = fromStr
        whereLabel.text = whereStr
        indexPath = index
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cookerReady.text = items[indexPath].cookerReady ? "Да" : "Нет"
        if analytic[indexPath].rider.contains(UserDefaults.standard.value(forKey: "nickname") as! String) {
            acceptButton.isUserInteractionEnabled = false
            acceptButton.alpha = 0
            endButton.alpha = 1
        }
        endButton.isUserInteractionEnabled = items[indexPath].cookerReady ? true : false
    }
}
